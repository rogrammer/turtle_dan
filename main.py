import turtle as tl
import turtle_library as tulib
import sys
import logging
import flask
import waitress
import json
import threading

def patch_translogger(app, accesslogger):
    import time
    from paste.translogger import TransLogger
 
    class CustomTransLogger (TransLogger):
 
        def write_log(self, environ, method, req_uri, start, status, bytes):
            if bytes is None:
                bytes = '-'
            if time.daylight:
                offset = time.altzone / 60 / 60 * -100
            else:
                offset = time.timezone / 60 / 60 * -100
            if offset >= 0:
                offset = "+%0.4d" % (offset)
            elif offset < 0:
                offset = "%0.4d" % (offset)
 
            remote_addr = '-'
            if environ.get ('HTTP_X_FORWARDED_FOR'):
                remote_addr = environ['HTTP_X_FORWARDED_FOR']
            elif environ.get ('HTTP_X_REAL_IP'):
                remote_addr = environ['HTTP_X_REAL_IP']
            elif environ.get ('REMOTE_ADDR'):
                remote_addr = environ['REMOTE_ADDR']
 
            d = {
                'REMOTE_ADDR': remote_addr,
                'REMOTE_USER': environ.get('REMOTE_USER', '-'),
                'REQUEST_METHOD': method,
                'REQUEST_URI': req_uri,
                'HTTP_VERSION': environ.get('SERVER_PROTOCOL'),
                'time': time.strftime('%d/%b/%Y:%H:%M:%S ', start) + offset,
                'status': status.split(None, 1)[0],
                'bytes': bytes,
                'HTTP_REFERER': environ.get('HTTP_REFERER', '-'),
                'HTTP_USER_AGENT': environ.get('HTTP_USER_AGENT', '-'),
            }
            message = self.format % d
            self.logger.log(self.logging_level, message)
 
 
    # override translogger access logs used by waitress
    translogger_format = \
        ('%(REMOTE_ADDR)s '
         '"%(REQUEST_METHOD)s %(REQUEST_URI)s %(HTTP_VERSION)s" '
         '%(status)s %(bytes)s "%(HTTP_REFERER)s" "%(HTTP_USER_AGENT)s"')
    return CustomTransLogger (app
        , logger = accesslogger
        , format = translogger_format
        , setup_console_handler = False
    )

def parse_arguments(a):	
	if a[1]=='local':
		return True
	elif a[1]=='remote':
		return False
	return True                                

def main():

#initialisierung
	logging.basicConfig(level=logging.DEBUG)
	log=logging.getLogger('main')
	log.debug ('initializing Turtle')
	tl.setup (500, 500)
	s=tl.Screen()
	schildi=tl.Turtle()
	schildi.speed(0)
	#tl.showturtle()

#Registierung
	log.debug ('defining handle functions')
	def handle_up():
		tulib.up(schildi)
	def handle_down(): 
		tulib.down(schildi)
	def handle_left():
		tulib.left(schildi)
	def handle_right():
		tulib.right(schildi)
	def handle_home():
		tulib.home(schildi)
	def handle_clear():
		tulib.clear(schildi)
	def handle_reset():
		tulib.reset(schildi)
	def handle_green():
		tulib.green(schildi)
	def handle_red():
		tulib.red(schildi)
	def handle_blue():
		tulib.blue(schildi)
	def handle_yellow():
		tulib.yellow(schildi)

	log.debug ('register_keyboard_handling')
	def register_keyboard_handling():
		log.debug('turtle_listen')
		s.listen()
		log.debug('registering local key handler')
		s.onkey(handle_up, 'Up') 
		s.onkey(handle_down, 'Down')
		s.onkey(handle_left, 'Left')
		s.onkey(handle_right, 'Right')
		s.onkey(handle_home, 'h')
		s.onkey(handle_clear, 'c')
		s.onkey(handle_reset, 'q')
		s.onkey(handle_green, 'g')
		s.onkey(handle_red, 'r')
		s.onkey(handle_blue, 'b')
		s.onkey(handle_yellow, 'y')

	log.debug('register_remote_handling')	
	def register_remote_handling():
		log.debug ('registering handle up')
		def handle_move_up():
			log.debug('handle_up')
			handle_up()
			log.debug('respnse_up')
			return flask.Response (status = 200)
		def handle_move_down():
			log.debug('handle_down')
			handle_down()
			log.debug('response_down')
			return flask.Response (status = 200)
		def handle_reset_request():
			log.debug('handle_reset')
			handle_reset()
			log.debug('respnse_reset')
			return flask.Response (status = 200)

		log.debug ('creating server')
		web=flask.Flask('remote_turtle')
		log.debug('web_url')
		web.add_url_rule('/up', 'move_up', handle_move_up)
		web.add_url_rule('/down', 'move_down', handle_move_down)
		web.add_url_rule('/reset', 'reset_tl', handle_reset_request)
		log.debug ('Defining server thread function ...')
		def run_app_thread (flask_app, host, port):
			log.debug ('Starting waitress server ...')
			waitress.serve (flask_app, host = host, port = port)
		log.debug ('Creating server thread ...')
		app_runner = threading.Thread (target = run_app_thread,
			 args = [web, '0.0.0.0', 7777])
		log.debug ('Starting server thread ...')
		app_runner.start ()

	log.debug ('checking arguments')
	is_local=True
	if 1 < len (sys.argv):
		is_local=parse_arguments(sys.argv)
	log.debug ('registering handlers')
	
	if is_local: 
		log.debug ('local')
		register_keyboard_handling()
	
	else: 
		log.debug ('remote')
		register_remote_handling()

	log.debug('mainloop') 
	#tl.mainloop()
	s.mainloop()


if __name__ == '__main__':
	print ('calling main')
	main()