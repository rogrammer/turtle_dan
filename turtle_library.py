def home(t):
	t.home()

def clear(t):
	t.clear()

def reset(t):
	t.home()
	t.clear()

def up(t):
	t.setheading(90)
	t.forward(50)
	print ('up')

def down(t):
	t.setheading(270)
	t.forward(50)
	print ('down')

def left(t):
	t.setheading(180)
	t.forward(50)
	print ('left')

def right(t):
	t.setheading(0)
	t.forward(50)
	print ('right')

def red(t):
	t.color("red")

def green(t):
	t.color("green")

def blue(t):
	t.color("blue")

def yellow(t):
	t.color("yellow")